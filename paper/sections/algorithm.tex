%%% algorithm

\section{Algorithm}
\label{sec:algorithm}

This study aims to accelerate a process to identify moving object candidates from a sequence of images. Many sophisticated applications are available to extract sources from images \citep[e.g., \texttt{DAOFIND} and \texttt{Source Extractor}; ][]{stetson_daophot_1987,bertin_sextractor:_1996}. The list of the extracted sources may contain fixed stars, cosmic rays, and artificial noises, as well as possible moving objects. It is not an easy task to isolate moving object candidates from such distractors. This process can be a bottleneck in moving object extraction.

We developed a dedicated algorithm, Tracklet Extraction Engine (\texttt{tracee}), which efficiently extracts moving object candidates from a source list. The \texttt{tracee} is originally developed for an asteroid search but is possibly utilized in other fields. Table~\ref{tab:appendix:tutorial} describes a basic usage of \texttt{tracee}.


\subsection{Problem Setting}
\label{sec:algorithm:problem}

The algorithm we develop, \texttt{tracee},  is intended to deal with short video data. Thus, we assume that the motions of moving objects are linear and uniform. Thus, the problem is described as extracting linearly aligned points from a point cloud in a three-dimensional space. A single data set may contain multiple moving objects. The algorithm is required to detect them separately. The moving objects possibly cross fixed stars and each other. The algorithm should work properly in such cases.

\textit{Hough transformation} \citep{hough_method_1962} a plausible algorithm to deal with the problem, where each source votes for points in the hyperspace where each point is associated with a line segment. Such voting-based algorithms are, however, usually memory-consuming, while some efficient implementations are proposed \citep{li_fast_1986}. Thus, we do not adopt Hough transformation. Instead, we split the problem into two parts; First, we construct a $k$-nearest neighbor graph of the sources and then extract linearly aligned edges from the graph. An overview of the procedures is presented in Algorithm~\ref{algorithm:tracee}. Detailed descriptions are described below.


\begin{algorithm}[tp]
  \caption{Overview of \texttt{tracee}}
  \label{algorithm:tracee}
  \DontPrintSemicolon
  \SetKwProg{Part}{}{}{}
  \Part{\textbf{\upshape Part 1:} $k$-Nearest Neighbor Graph}{
    \Input{$V=\left\{v_i\middle|_{i=1{\ldots}N}\right\}$}
    \Output{$E=\left\{(v_i,v_j)\middle|_{i,j \in \{1{\ldots}N\}}\right\}$}
    {Construct a $k$-NN graph $G(V,\,E)$ from $V$.}\;
    {Merge the bidirectional edges in $E$.}\;
    {Redirect the edges in the chronological order.}\;
    {Remove inappropriate edges in $E$.}\;
    \KwRet{$E$}\;
  }
  \BlankLine
  \Part{\textbf{\upshape Part 2:} Line Segment Grouping}{
    \Input{$E=\left\{(v_i,v_j)\middle|_{i,j \in \{1{\ldots}N\}}\right\}$}
    \Output{$B=\left\{b_m\middle|_{m=1{\ldots}M}\right\}$}
    {Construct a set of baselines $B$ from $E$.}\;
    {Remove short baselines from $B$.}\;
    {Remove baselines with large scatter from $B$.}
    \KwRet{$B$}\;
  }
\end{algorithm}


\subsection{Procedures}
\label{sec:algorithm:procedure}

\subsubsection{Input and Output}
\label{sec:algorithm:procedure:input/output}

The \texttt{tracee} receives a list containing the source positions and the timestamps of extraction. Hereafter, we call an element of the list a \textit{vertex}. The input data set $V$ is described as follows:
\begin{equation}
  \label{eq:algo:vertex}
  V = \Bigl\{
    v_i = \left\{ x_i, \, y_i, \, t_i \right\} \Bigm|
    i = 0,\ldots,N
    \Bigr\},
\end{equation}
where $i$ is the index of the source, $v_i$ is the vertex corresponding to the $i$th source, $x_i$ and $y_i$ are the source positions, $t_i$ is the extraction timestamp, and $N$ is the total number of the extracted sources.

The tracklet is defined as a line segment or an edge. We define a \textit{baseline} as a data structure containing a line segment and associated vertices:
\begin{equation}
  \label{eq:algo:tracklet}
    b = \left\{ v^s, v^e, w \right\},
\end{equation}
where $v^s$ and $v^e$ are respectively the starting and end points of the line segment, $w$ contains the list of the vertices associated with the baseline. The definitions of these elements are described below:
\begin{equation}
  \label{eq:algo:tracklet:detail}
  \begin{cases}
    v_s = \left\{ x^s,\, y^s,\, t^s \right\}, \\
    v_e = \left\{ x^e,\, y^e,\, t^e \right\}, \\
    w = \left\{ \theta_1,\, \theta_2,\, \ldots,\, \theta_{K} \right\},
  \end{cases}
\end{equation}
where $K$ is the number of the vertices associated with the baseline and $\theta_i$ denotes the index of the vertex. The algorithm is expected to return a set of baselines, $B$:
\begin{equation}
  \label{eq:algo:baseline}
  B = \Bigl\{
    b_m = \left\{ v^\mathrm{s}_m,\, v^\mathrm{e}_m,\, w_m \right\}
    \Bigm| m = 0,\ldots,M
  \Bigr\},
\end{equation}
where $m$ is the index of the baseline and $M$ is the total number of the baselines.

\subsubsection{$k$-Nearest Neighbor Graph}
\label{sec:algorithm:procedure:knn}

In the first part of the algorithm, we construct a $k$-nearest neighbor graph ($k$-NN graph) from the vertices. Every vertex of the $k$-NN graph has at least $k$ edges to the $k$th nearest vertices. Samples of the $k$-NN graph are presented in Figure~\ref{fig:algo:knn_sample}. The blue dots represent the vertices. The solid and dashed lines show the $k$-NN graphs for $k=1$ and $3$, respectively.

\begin{figure}[t]
  \centering
  \includegraphics[width=\linewidth]{./figs/knn_sample.pdf}
  \caption{A sample of $k$-nearest neighbor graph. The blue dots indicate the vertices. The nearest neighbor graph ($k{=}1$) is denoted by the red thick lines. The $3$-nearest neighbor graph is shown by the gray dashed lines.}
  \label{fig:algo:knn_sample}
\end{figure}

A brute-force construction of a $k$-NN graph requires a cost of $O(n^{2})$, which is not acceptable for large problems. Dong et al. (2011) presented \texttt{NN-Descent}, an efficient algorithm to obtain an approximate $k$-NN graph, where the construction cost is about $O(n^{1.14})$ and is suitable for parallel computing. An overview of \texttt{NN-Descent} is described in Algorithm~\ref{algorithm:nndescent}. The algorithm receives the vertex set $V$ and the order of the graph $k$. The edge list $E[v]$ handles the edges starting from $v$. The element of $E[v]$ is a pair of the destination $w$ and the distance between $v$ and $w$. At first, $E[v]$ is initialized with arbitrary $k$ vertices with infinity distances. Then, the edge list $E$ is iteratively updated by replacing elements with shorter ones. The algorithm stops when no element is updated.

\begin{algorithm}[t]
  \caption{\texttt{NN-Descent}}
  \label{algorithm:nndescent}
  \SetKwFor{Loop}{loop}{}{}
  \DontPrintSemicolon
  \Input{vertex set $V$, order $k$}
  \Output{$k$-NN edge list $E$}
  \BlankLine
  \ForEach(\tcp*[f]{initialize $E$}){$v \in V$}{
    {$W \gets$ arbitrary $K$ elements from $V$.}\;
    \lForEach{$w \in W$}{append $\langle w, \infty\rangle$ to $E[v]$.}
  }
  \Loop(\tcp*[f]{iteratively update $E$}){}{
    {$R \gets$ reverse graph of $E$.}\;
    \lForEach{$v \in V$}{$\overline{E}[v] \gets E[v] \cup R[v]$.}
    {$c \gets \mathit{False}$}\;
    \ForEach{$v \in V$}{
      \ForEach{$u_1 \in \overline{E}[v]$, $u_2 \in \overline{E}[u_1]$}{
        {$l \gets$ distance between $v$ and $u_2$}\;
        {$\langle \tilde{v},s \rangle \gets$ largest element of $E[v]$.}\;
        \If{$l < s$}{
          {pop $\langle \tilde{v},s \rangle$ from $E[v]$.}\;
          {append $\langle u_2,l \rangle$ into $E[v]$.}\;
          {$c \gets \mathit{True}$}\;
        }
      }
    }
    \lIf{$c = \mathit{False}$}{\KwRet{$E$}}
  }
\end{algorithm}

\texttt{NN-Descent} has been widely used, and there are already several implementations. While major implementations are well-established and functionally rich, high performance is required by tracee. Thus, we have developed a minimal package to construct a $k$-NN graph efficiently in a three-dimensional Euclidian space \citep{ohsawa_minimalknn_2020}. The core function of the module is written in \texttt{C++}, and an interface for \texttt{Python} is provided. The package, \texttt{minimalKNN}, is available in the Python Package Index as open-source software.

The output of Algorithm~\ref{algorithm:nndescent} is a directed graph. The edges should be in chronological order to trace moving objects. Thus, the bidirectional edges are merged, and the edges are redirected in chronological order. We also remove inappropriate edges whose corresponding velocities are too high. Then, the updated $k$-NN graph is passed to further analysis.


\subsubsection{Line Segment Grouping}
\label{sec:algorithm:procedure:grouping}

Linearly aligned edges of the graph are extracted to identify moving object candidates. \citet{jang_fast_2002} tackled the problem of extracting favorable line segments from a gray-scale image. The proposed algorithm is a fast line segment grouping method; First, edges are extracted by an edge detector. Then, the edges are split into short line segments (hereafter, elementary line segments, ELSs). The algorithm they developed extracts a subset of linearly aligned line segments from a set of ELSs. An overview of the algorithm is described in Algorithm~\ref{algorithm:fdlsgm}.

\begin{algorithm}[t]
  \caption{\texttt{fast ELS grouping}}
  \label{algorithm:fdlsgm}
  \SetKwFor{Loop}{loop}{}{}
  \DontPrintSemicolon
  \Input{set of ELSs $E$}
  \Output{set of baselines $B$}
  \BlankLine
  {construct an empty \textit{accumulator} $A$.}\;
  \ForEach(\tcp*[f]{initialization}){$e \in E$}{
    {$\theta \gets \mathrm{argument}(e)$}\;
    {$c \gets \mathit{False}$}\;
    \ForEach{$b \in A[\theta]$}{
      \If{$e \sim b$} {
        {append $e$ to the members of $b$}\;
        {$c \gets \mathrm{True}$}
      }
    }
    \If{$c = \mathit{False}$}{
      {generate a baseline $b'$ from $e$}\;
      {append $b'$ to $A[\theta]$.}}
  }
  \Loop(\tcp*[f]{optimization}){}{
    \ForEach{$e \in E$}{
      {$\theta \gets \mathrm{argument}(e)$}\;
      {$c \gets \mathit{False}$}\;
      \ForEach{$b \in A[\theta]$}{
        \If{$e \sim b$ \& $e \not\in b$}{
          {append $e$ to the members of $b$}\;
          {$c \gets \mathrm{True}$}
        }
      }
    }
  }
  {construct an empty \textit{baseline set} $\overline{B}$}\;
  \ForEach(\tcp*[f]{merge baselines}){$b \in B$}{
    {$\theta \gets \mathrm{argument}(b)$}\;
    \ForEach{$\tilde{b} \in A[\theta]$}{
      \If{$b \sim \tilde{b}$}{
        {$b \gets \mathrm{merge}(b,\tilde{b})$}\;
        {remove $b$ and $\tilde{b}$ from $B$}\;
      }
    }
    {append $b$ into $\overline{B}$}\;
  }
  {$B \gets \overline{B}$}\;
\end{algorithm}

\begin{figure}
  \centering
  \includegraphics[width=.9\linewidth]{./figs/fdslgm_similarity.pdf}
  \caption{A schematic view of the line segment grouping criteria. The blue arrow indicates the baseline, while the short black arrows indicate the elemental line segments.}
  \label{fig:algo:fdlsgm:criteria}
\end{figure}

\textit{Baselines} and an \textit{accumulator} play an essential role in facilitating line segment grouping. The Baselines are line segment candidates obtained by grouping colinear ELSs, while the accumulator is a data structure to search baselines by the position angle. First, each ELS is compared with baselines in the accumulator. Then, if the ELS and the baseline fulfill the similarity criterion, the baseline is updated by embracing the ELS. Otherwise, a new baseline is created from the ELS. Refer to \citet{jang_fast_2002} for the way to create and update baselines. The definition of the similarity is illustrated in Figure~\ref{fig:algo:fdlsgm:criteria}. The \textit{argument} is the angle between the ELS and the baseline, the \textit{lateral distance} is the distance perpendicular to the baseline, and the \textit{gap distance} is the separation between the ELS and the baseline projected on the baseline. Second, the same procedure is repeated one more time, but no new baseline is created this time. Finally, similar baselines are merged into a single baseline.

The original algorithm in \citet{jang_fast_2002} works on non-directional line segments on the two-dimensional plane. Thus, we have extended the algorithm to directional line segments in the three-dimensional space. The extended algorithm is named \textit{Fast Directed Line Segment Grouping Method} (\texttt{}fdlsgm). The developed code is published as open-source software \citep{ohsawa_fdlsgm_2020}. The core function of the module is written in C++, and an interface for Python is implemented.

The output of fdlsgm may contain unsuccessful baselines which are stochastically grouped as lines. First, the baselines are removed when the number of associated vertices is small. Then, the scatter of the vertices around the baseline is evaluated, and the baselines with large scatter are removed. The remaining baselines are finally returned as the candidates of tracklets.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
